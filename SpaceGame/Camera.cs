﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceGame
{
    public struct Camera
    {
        public Matrix ViewMatrix;
        public Matrix ProjectionMatrix;
        public GraphicsDevice GraphicsDevice;
        public SpriteBatch SpriteBatch;
        public GameWindow Window;
        public bool HasMouse;
    }
}
