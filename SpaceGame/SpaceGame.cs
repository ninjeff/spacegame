﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace SpaceGame
{
    public sealed class SpaceGame : Game
    {
        private readonly GraphicsDeviceManager _Graphics;
        private readonly GameState[] _GameStates;
        private readonly Star[] _Stars;
        private readonly Obj[] _Objects;
        private readonly Camera[] _Cameras;
        private readonly List<IProcessor> _Processors;

        public SpaceGame()
        {
            _Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _GameStates = new GameState[1];
            _Stars = new Star[800];
            _Objects = new Obj[20];
            _Cameras = new Camera[1];

            _Processors = new List<IProcessor>()
            {
                new UpdateProcessor(_GameStates, _Cameras, _Objects),
                new RenderProcessor(_Cameras, _Stars, _Objects)
            };
        }

        protected override void Initialize()
        {
            base.Initialize();

            Window.IsBorderless = true;
            _Graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            _Graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            _Graphics.ApplyChanges();

            var starTexture = new Texture2D(GraphicsDevice, 1, 1);
            var data = new Color[1];
            data[0] = Color.White;
            starTexture.SetData(data);

            var random = new Random();
            for (var i = 0; i < _Stars.Length; ++i)
            {
                var u = (float)random.NextDouble();
                var v = (float)random.NextDouble();
                var theta = MathHelper.TwoPi * u;
                var phi = (float)Math.Acos(2 * v - 1);
                _Stars[i] = new Star()
                {
                    Direction = new Vector4((float)(Math.Cos(theta) * Math.Sin(phi)), (float)(Math.Sin(theta) * Math.Sin(phi)), (float)Math.Cos(phi), 0.0f),
                    Texture = starTexture
                };
            }

            var basicEffect = new BasicEffect(GraphicsDevice);

            var vertices = new VertexPositionColor[12];
            vertices[0] = new VertexPositionColor(new Vector3(-0.26286500f, 0.0000000f, 0.42532500f), Color.Red);
            vertices[1] = new VertexPositionColor(new Vector3(0.26286500f, 0.0000000f, 0.42532500f), Color.Orange);
            vertices[2] = new VertexPositionColor(new Vector3(-0.26286500f, 0.0000000f, -0.42532500f), Color.Yellow);
            vertices[3] = new VertexPositionColor(new Vector3(0.26286500f, 0.0000000f, -0.42532500f), Color.Green);
            vertices[4] = new VertexPositionColor(new Vector3(0.0000000f, 0.42532500f, 0.26286500f), Color.Blue);
            vertices[5] = new VertexPositionColor(new Vector3(0.0000000f, 0.42532500f, -0.26286500f), Color.Indigo);
            vertices[6] = new VertexPositionColor(new Vector3(0.0000000f, -0.42532500f, 0.26286500f), Color.Purple);
            vertices[7] = new VertexPositionColor(new Vector3(0.0000000f, -0.42532500f, -0.26286500f), Color.White);
            vertices[8] = new VertexPositionColor(new Vector3(0.42532500f, 0.26286500f, 0.0000000f), Color.Cyan);
            vertices[9] = new VertexPositionColor(new Vector3(-0.42532500f, 0.26286500f, 0.0000000f), Color.Black);
            vertices[10] = new VertexPositionColor(new Vector3(0.42532500f, -0.26286500f, 0.0000000f), Color.DodgerBlue);
            vertices[11] = new VertexPositionColor(new Vector3(-0.42532500f, -0.26286500f, 0.0000000f), Color.Crimson);

            var vertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColor), 12, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices);

            var indices = new short[60];
            indices[0] = 0; indices[1] = 6; indices[2] = 1;
            indices[3] = 0; indices[4] = 11; indices[5] = 6;
            indices[6] = 1; indices[7] = 4; indices[8] = 0;
            indices[9] = 1; indices[10] = 8; indices[11] = 4;
            indices[12] = 1; indices[13] = 10; indices[14] = 8;
            indices[15] = 2; indices[16] = 5; indices[17] = 3;
            indices[18] = 2; indices[19] = 9; indices[20] = 5;
            indices[21] = 2; indices[22] = 11; indices[23] = 9;
            indices[24] = 3; indices[25] = 7; indices[26] = 2;
            indices[27] = 3; indices[28] = 10; indices[29] = 7;
            indices[30] = 4; indices[31] = 8; indices[32] = 5;
            indices[33] = 4; indices[34] = 9; indices[35] = 0;
            indices[36] = 5; indices[37] = 8; indices[38] = 3;
            indices[39] = 5; indices[40] = 9; indices[41] = 4;
            indices[42] = 6; indices[43] = 10; indices[44] = 1;
            indices[45] = 6; indices[46] = 11; indices[47] = 7;
            indices[48] = 7; indices[49] = 10; indices[50] = 6;
            indices[51] = 7; indices[52] = 11; indices[53] = 2;
            indices[54] = 8; indices[55] = 10; indices[56] = 3;
            indices[57] = 9; indices[58] = 11; indices[59] = 0;

            var indexBuffer = new IndexBuffer(_Graphics.GraphicsDevice, typeof(short), indices.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData(indices);

            for (var i = 0; i < _Objects.Length; ++i)
            {
                _Objects[i] = new Obj()
                {
                    Position = new Vector4((float)random.NextDouble() * 100 - 50, (float)random.NextDouble() * 100 - 50, (float)random.NextDouble() * 100 - 50, 1),
                    Orientation = Quaternion.Identity,
                    AngularVelocity = new Vector3((float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1),
                    Effect = basicEffect,
                    VertexBuffer = vertexBuffer,
                    IndexBuffer = indexBuffer
                };
            }

            _Cameras[0] = new Camera()
            {
                ViewMatrix = Matrix.CreateLookAt(new Vector3(0, 0, 0), new Vector3(0, 0, -1), new Vector3(0, 1, 0)),
                ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 800f / 480f, 0.01f, 100f),
                GraphicsDevice = GraphicsDevice,
                SpriteBatch = new SpriteBatch(GraphicsDevice),
                Window = Window,
                HasMouse = true
            };
        }

        protected override void Draw(GameTime gameTime)
        {
            foreach (var processor in _Processors)
            {
                processor.Execute(gameTime);
            }

            for (var i = 0; i < _GameStates.Length; ++i)
            {
                if (_GameStates[i].ShouldExit)
                {
                    Exit();
                    return;
                }

                _GameStates[i].IsActive = IsActive;
            }

            base.Draw(gameTime);
        }
    }
}
