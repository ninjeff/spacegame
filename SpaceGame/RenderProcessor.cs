﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceGame
{
    public sealed class RenderProcessor : IProcessor
    {
        private readonly Camera[] _Cameras;
        private readonly Star[] _Stars;
        private readonly Obj[] _Objects;

        public RenderProcessor(
            Camera[] cameras,
            Star[] stars,
            Obj[] objects)
        {
            _Cameras = cameras;
            _Stars = stars;
            _Objects = objects;
        }

        public void Execute(GameTime gameTime)
        {
            foreach (var camera in _Cameras)
            {
                camera.GraphicsDevice.Clear(Color.Black);
                camera.SpriteBatch.Begin();
                foreach (var star in _Stars)
                {
                    var direction = Vector4.Transform(star.Direction, camera.ViewMatrix * camera.ProjectionMatrix);
                    direction /= direction.W;
                    camera.SpriteBatch.Draw(star.Texture, new Vector2((direction.X / 2 + 0.5f) * camera.Window.ClientBounds.Width, (-direction.Y / 2 + 0.5f) * camera.Window.ClientBounds.Height), Color.White);
                }
                camera.SpriteBatch.End();

                camera.GraphicsDevice.BlendState = BlendState.Opaque;
                camera.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

                foreach (var obj in _Objects)
                {
                    camera.GraphicsDevice.SetVertexBuffer(obj.VertexBuffer);
                    camera.GraphicsDevice.Indices = obj.IndexBuffer;

                    obj.Effect.View = camera.ViewMatrix;
                    obj.Effect.Projection = camera.ProjectionMatrix;
                    obj.Effect.World = Matrix.CreateFromQuaternion(obj.Orientation) * Matrix.CreateTranslation(obj.Position.X / obj.Position.W, obj.Position.Y / obj.Position.W, obj.Position.Z / obj.Position.W);
                    obj.Effect.VertexColorEnabled = true;

                    camera.GraphicsDevice.RasterizerState = new RasterizerState
                    {
                        CullMode = CullMode.CullClockwiseFace
                    };

                    foreach (var pass in obj.Effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        camera.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 20);
                    }
                }
            }
        }
    }
}
