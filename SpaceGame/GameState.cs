﻿namespace SpaceGame
{
    public struct GameState
    {
        public bool IsActive;
        public bool ShouldExit;
    }
}
