﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceGame
{
    public sealed class Obj
    {
        public Vector4 Position;
        public Quaternion Orientation;
        public Vector3 AngularVelocity;
        public BasicEffect Effect;
        public VertexBuffer VertexBuffer;
        public IndexBuffer IndexBuffer;
    }
}
