﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceGame
{
    public struct Star
    {
        public Vector4 Direction;
        public Texture2D Texture;
    }
}
