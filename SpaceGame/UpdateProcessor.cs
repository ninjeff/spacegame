﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpaceGame
{
    public sealed class UpdateProcessor : IProcessor
    {
        private readonly GameState[] _GameStates;
        private readonly Camera[] _Cameras;
        private readonly Obj[] _Objects;

        public UpdateProcessor(
            GameState[] gameStates,
            Camera[] cameras,
            Obj[] objects)
        {
            _GameStates = gameStates;
            _Cameras = cameras;
            _Objects = objects;
        }

        public void Execute(GameTime gameTime)
        {
            var dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            var keyboard = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyboard.IsKeyDown(Keys.Escape))
            {
                _GameStates[0].ShouldExit = true;
            }

            var movementDirection = new Vector3();
            if (keyboard.IsKeyDown(Keys.W))
            {
                movementDirection += new Vector3(0, 0, 1);
            }
            if (keyboard.IsKeyDown(Keys.S))
            {
                movementDirection += new Vector3(0, 0, -1);
            }
            if (keyboard.IsKeyDown(Keys.A))
            {
                movementDirection += new Vector3(1, 0, 0);
            }
            if (keyboard.IsKeyDown(Keys.D))
            {
                movementDirection += new Vector3(-1, 0, 0);
            }
            if (keyboard.IsKeyDown(Keys.Space))
            {
                movementDirection += new Vector3(0, -1, 0);
            }
            if (keyboard.IsKeyDown(Keys.X))
            {
                movementDirection += new Vector3(0, 1, 0);
            }

            for (var i = 0; i < _Cameras.Length; ++i)
            {
                UpdateCamera(ref _Cameras[i], movementDirection, dt);
            }

            for (var i = 0; i < _Objects.Length; ++i)
            {
                UpdateObject(ref _Objects[i], dt);
            }
        }

        private void UpdateCamera(ref Camera camera, Vector3 movementDirection, float dt)
        {
            if (movementDirection.LengthSquared() != 0)
            {
                movementDirection.Normalize();

                var velocity = movementDirection * 10f;
                var displacement = velocity * dt;

                camera.ViewMatrix *= Matrix.CreateTranslation(displacement);
            }

            if (camera.HasMouse && _GameStates[0].IsActive)
            {
                var mouse = Mouse.GetState();
                var yaw = (mouse.X - camera.Window.ClientBounds.Center.X) * 0.5f * dt;
                var pitch = (mouse.Y - camera.Window.ClientBounds.Center.Y) * 0.5f * dt;
                Mouse.SetPosition(camera.Window.ClientBounds.Center.X, camera.Window.ClientBounds.Center.Y);
                camera.ViewMatrix *= Matrix.CreateFromYawPitchRoll(yaw, pitch, 0);
            }
        }

        private void UpdateObject(ref Obj obj, float dt)
        {
            var angularDisplacement = obj.AngularVelocity * dt;
            obj.Orientation *= Quaternion.CreateFromYawPitchRoll(angularDisplacement.Y, angularDisplacement.X, angularDisplacement.Z);
        }
    }
}
