﻿using Microsoft.Xna.Framework;

namespace SpaceGame
{
    public interface IProcessor
    {
        void Execute(GameTime gameTime);
    }
}
