﻿using System;

namespace SpaceGame
{
    public static class Program
    {
        [STAThread()]
        public static void Main()
        {
            using (var game = new SpaceGame())
            {
                game.Run();
            }
        }
    }
}
